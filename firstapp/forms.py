from django.core.validators import validate_email, validate_slug
from django import forms

# validations min length ,maxlength etc


class StudentRegistration(forms.Form):  # or forms.ModelForm
    # CASE 1 :   strip space lyi

    name = forms.CharField(min_length=5, max_length=30, strip=False)

    # CASE  2 : empty value---> this field is required na awe ,jdo mae koi value na de k direct submit te click kra te

    '''name = forms.CharField(empty_value='sonam') '''

    # CASE 3 : This field is required likya by default django vlu ,odi jda kh=huch hor likhna howe

    '''name = forms.CharField(error_messages={'required': 'Name is mandatory'})'''

    # INTEGER FIELD VALIDATIONS

    # CASE 1: arrow aune by default oh asi 1,2,-1 etc vda ghta sakde arrow te click karke

    rollno = forms.IntegerField()

    # CASE 2: ARROW AUNE BUT jini min value diti othu strt hone 1,2 -1,etc toh by default ni aune

    rollno = forms.IntegerField(min_value=5, max_value=7)

    # DECIMAL FIELD VALIDATIONS

    # CASE 1 :

    price = forms.DecimalField(
        min_value=5, max_value=7, max_digits=4, decimal_places=1)

    # FLOAT FIELD VALIDATIONS

    # CASE 1: IMP----> decimal ch int value leye ta int hi print hundi but float ch int value len te .0 v print hunda nal
    # EXMAPLE :  input 12 lye thn decimal ch 12 print hoya ,BUT float ch 12.0 print hona
    rate = forms.FloatField(min_value=5, max_value=10)

    # OTHER FIELD VALIDATIONS

    comment = forms.SlugField()
    email = forms.EmailField(min_length=5, max_length=15)
    website = forms.URLField(min_length=5, max_length=15)
    password = forms.CharField(
        min_length=5, max_length=15, widget=forms.PasswordInput)
    description = forms.CharField(widget=forms.Textarea)
    feedback = forms.CharField(min_length=5, max_length=15, widget=forms.TextInput(
        attrs={'class': 'somecss1 somecss2', 'id': 'uniqueid'}))
    notes = forms.CharField(widget=forms.Textarea(
        attrs={'rows': 3, 'cols': 10}))

    # BOOLEAN FIELD VALIDATIONS

    # CASE 1 : BOOLEN FIELD VALIDATIONS (bydefault THIS FILD IS REQUIRED AUNA H)

    ''' agree = forms.BooleanField() '''

    # CASE 2 : ede ch bydefalut : a rhe

    agree = forms.BooleanField(label='I AGREE')

    # CASE 3 : ede ch : colon remove kite labelsuffix nal

    '''agree = forms.BooleanField(label_suffix='', label='I AGREE')'''
